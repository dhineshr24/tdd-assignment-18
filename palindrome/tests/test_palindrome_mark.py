from palindrome.app.palindrome import is_palindrome
import pytest


@pytest.fixture
def input_from_file_one():
    file = open("/Users/shivamraj/Documents/Learning/pytest/palindrome/resources/file_one.txt", "r")
    examples = file.read()
    return examples


@pytest.fixture
def output_from_file_one():
    file = open("/Users/shivamraj/Documents/Learning/pytest/palindrome/resources/expected_file_one.txt", "r")
    examples = file.read()
    return examples


def test_is_palindrome_from_file_one(input_from_file_one, output_from_file_one):
    assert str(is_palindrome(input_from_file_one)) == output_from_file_one


@pytest.mark.parametrize("palindrome", [
    "",
    "a",
    "Bob",
    "Never odd or even",
    "Do geese see God?",
])
def test_is_palindrome(palindrome):
    assert is_palindrome(palindrome)


@pytest.fixture
def input_from_file_two():
    file = open("/Users/shivamraj/Documents/Learning/pytest/palindrome/resources/file_two.txt", "r")
    examples = file.read()
    return examples


@pytest.fixture
def output_from_file_two():
    file = open("/Users/shivamraj/Documents/Learning/pytest/palindrome/resources/expected_file_two.txt", "r")
    examples = file.read()
    return examples


def test_is_not_palindrome_from_file_two(input_from_file_two, output_from_file_two):
    assert str(is_palindrome(input_from_file_two)) == output_from_file_two


@pytest.mark.parametrize("non_palindrome", [
    "abcd",
    "ababab",
])
def test_is_palindrome_not_palindrome(non_palindrome):
    assert not is_palindrome(non_palindrome)


@pytest.mark.parametrize("maybe_palindrome, expected_result", [
    ("", True),
    ("A", True),
    ("Bob", True),
    ("Never odd or even", True),
    ("Do geese see God?", True),
    ("abcd", False),
    ("ababab", False),
])
def test_is_palindrome(maybe_palindrome, expected_result):
    assert is_palindrome(maybe_palindrome) == expected_result


@pytest.mark.parametrize("example, expected", [
    ('deleveled', True),
    ('Malayalam', True),
    ('detartrated', True),
    ('a', True),
    ('repaper', True),
    ('Al lets Della call Ed Stella', True),
    ('Lisa Bonet ate no basil', True),
    ('Linguistics', False),
    ('Python', False),
    ('palindrome', False),
    ('an', False),
    ('re-paper', True)
])
def test_palindrome_detector_looping_(example, expected):
    result = is_palindrome(example)
    assert result == expected

# Function which return reverse of a string
# And checks if both are same or not

# def is_palindrome(s):
#     s = s.lower()
#     return s == s[::-1]

import re


def is_palindrome(s):
    s = s.lower()
    # A function that strips all non-letters,
    # lowercase the result and then only compares 
    # the initial with the final character would survive the tests
    tidied = re.sub('[^A-Za-z]', '', s)
    return tidied == '' or tidied[0] == tidied[-1]

